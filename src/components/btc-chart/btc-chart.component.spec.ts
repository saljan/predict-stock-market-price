import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtcChartComponent } from './btc-chart.component';

describe('BtcChartComponent', () => {
  let component: BtcChartComponent;
  let fixture: ComponentFixture<BtcChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtcChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtcChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
