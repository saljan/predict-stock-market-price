import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AnnManagerService } from "../../providers/ann-manager/ann-manager.service";
import { DataManagerService } from "../../providers/data-manager.service";
import * as Plotly from 'plotly.js';

@Component({
  selector: 'puz-app-btc-chart',
  templateUrl: './btc-chart.component.html',
  styleUrls: ['./btc-chart.component.css']
})
export class BtcChartComponent implements OnInit {

  // ...
  @ViewChild('chart') el: ElementRef;

  // ...
  constructor(private AnnManagerService: AnnManagerService, private DataManagerService: DataManagerService) { }

  ngOnInit() {
    this.AnnManagerService.curentDataSet.subscribe(d => {
      this.DataManagerService.get_btc_data(d.start, d.end)
        .subscribe(res =>
          this.draw_btc_chart({
            price: Object.values(res['bpi']),
            date: Object.keys(res['bpi']),
            title: d.stock
          })
        );
    });
  }


  // ...
  draw_btc_chart(data) {
    const element = this.el.nativeElement;
    const formattedData = [{
      x: data.date,
      y: data.price,
      name: 'BTC',
      line: {
        color: 'red'
      }
    }];

    const style = {
      title: data.title,
      margin: { t: 60 },
      xaxis: { title: 'Date' },
      yaxis: { title: 'Price ($)' }
    }

    Plotly.newPlot(element, formattedData, style)
  }

}
