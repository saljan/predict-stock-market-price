import { Component, OnInit } from '@angular/core';
import { AnnManagerService } from "../../providers/ann-manager/ann-manager.service";

@Component({
  selector: 'puz-app-tab-4',
  templateUrl: './tab-4.component.html',
  styleUrls: ['./tab-4.component.css']
})
export class Tab4Component implements OnInit {

  // ...
  Dataset;
  ANN;
  learnigAlgo;

  // ...
  constructor(private AnnManagerService: AnnManagerService) { }

  ngOnInit() {
    this.AnnManagerService.curentDataSet.subscribe(d => this.Dataset = d);
    this.ANN = this.AnnManagerService.getSelectedANN();
    this.learnigAlgo = this.AnnManagerService.getSelectedLearningAlgo();
  }

}
