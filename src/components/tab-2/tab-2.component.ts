import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormArray } from '@angular/forms';
import { AnnDrawerService } from "../../providers/ann-drawer/ann-drawer.service";
import { AnnManagerService } from "../../providers/ann-manager/ann-manager.service";
import { ANN } from "../../model/ann";
import * as d3 from 'd3';


@Component({
  selector: 'puz-app-tab-2',
  templateUrl: './tab-2.component.html',
  styleUrls: ['./tab-2.component.css']
})
export class Tab2Component implements OnInit {

  // ...
  //  (neu_arr = [1, 1], bias = true, actv = 'sigmu') 
  /**
   * bias -> t./f.
   * in. num.
   * out. num.
   * lay. num. --> neu. num. per lay.
   * actv. func. 
   */

  // ...
  formGroup: FormGroup;
  ANN: ANN;
  activ_func = [
    { value: 'tanh', viewValue: 'Hyperbolic Tangent' },
    { value: 'sigmu', viewValue: 'Sigmoid' },
    { value: 'cos', viewValue: 'Cosinusoida' }
  ];
  margin = { left: 10, right: 10, top: 20, bottom: 20 };
  width = 700 - this.margin.left - this.margin.right;
  height = 500 - this.margin.top - this.margin.bottom;
  hidde = true;

  // ...
  constructor(private AnnManagerService: AnnManagerService, private AnnDrawerService: AnnDrawerService) { }

  ngOnInit() {
    this.formGroup = this.initFormGroup();
    this.onChange();
    this.ANN = this.AnnManagerService.getSelectedANN();
  }

  // ...
  initFormGroup(): FormGroup {
    return new FormGroup({
      bias: new FormControl(),
      input_num: new FormControl(),
      output_num: new FormControl(),
      layers_num: new FormControl(),
      actv_func: new FormControl()
    });
  }

  onSubmit(item) {
    //   this.AnnManagerService.setSelectedANN(item);
  }

  onHidde() {
    this.hidde = !this.hidde;
  }

  onChange() {
    this.formGroup.valueChanges.subscribe(d =>
      this.AnnDrawerService.draw_neurals(
        this.get_options(),
        this.ANN.input_num,
        this.ANN.layers_num.split(';').map(d => +d),
        this.ANN.output_num, []));
  }

  // private ...
  private get_svg() {
    if (document.getElementById('neural-svg') !== null)
      document.getElementById('neural-svg').innerHTML = '';
    return d3.select('#neural-svg')
      .append('svg')
      .attr('class', 'neural')
      .attr('width', this.width)
      .attr('height', this.height);
  };

  private get_options() {
    return {
      svg: this.get_svg(),
      width: this.width,
      height: this.height,
      stroke: 1
    }
  }

}
