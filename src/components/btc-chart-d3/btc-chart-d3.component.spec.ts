import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtcChartD3Component } from './btc-chart-d3.component';

describe('BtcChartD3Component', () => {
  let component: BtcChartD3Component;
  let fixture: ComponentFixture<BtcChartD3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtcChartD3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtcChartD3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
