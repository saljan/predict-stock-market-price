import { Component, OnInit } from '@angular/core';
import { DataManagerService } from "../../providers/data-manager.service";
import { AnnService } from "../../providers/ann.service";
import { DataSet } from "../../model/data-set";
import * as d3 from "d3";

@Component({
  selector: 'puz-app-btc-chart-d3',
  templateUrl: './btc-chart-d3.component.html',
  styleUrls: ['./btc-chart-d3.component.css']
})
export class BtcChartD3Component implements OnInit {

  // ...
  DataSet: DataSet;
  stock_logo = [];
  btc_chart = [];

  // ...
  constructor(private AnnService: AnnService, private DataManagerService: DataManagerService) { }

  ngOnInit(): void {
    this.stock_logo = this.DataManagerService.getStocks();
    this.DataSet = this.AnnService.getSelectedData();
   // this.draw_chart_btc_chart();
  }

  // ...
 /* draw_chart_btc_chart() {
    this.btc_chart = [];
    this.AnnService.setSelectedData(this.DataSet);

    this.DataManagerService.get_btc_data(this.DataSet.start, this.DataSet.end)
      .subscribe(res => {
        // let price = Object.values(res['bpi']),
          // date = Object.keys(res['bpi']).map(d => new Date(d).toLocaleTimeString('pl', { year: 'numeric', month: 'short', day: 'numeric' }));  
          // date = Object.keys(res['bpi']).map(d => new Date(d));        
        let data = Object.keys(res['bpi']).map((d, i) => {
            return {
              price: Object.values(res['bpi'])[i],
              date: new Date(d)
            };
          });
        this.draw_chart(data, '.chart-container');
      });

  }*/
/*
  draw_chart(data, selector) {
    // ...
    d3.select(selector).html('');
    // ...
    let margin = { top: 20, right: 20, bottom: 50, left: 40 },
      width = 700 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom,
      svg = d3.select(selector)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .attr('class', 'btc-svg'),
      g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // console.log(data)


    let x = d3.scaleTime()
      .domain(d3.extent(data, (d) => { return d.date; }))
      .rangeRound([0, width]);

    let y = d3.scaleLinear()
      .domain([d3.min(data.map( d => d.price)), d3.max(data.map( d => d.price))])
      .rangeRound([height, 0]);

    let line = d3.line()
      .x((d) => { return x(d.date); })
      .y((d) => { return y(d.price); });


    g.append("text")
      .attr("x", (width / 2))
      .attr("y", (margin.top / 2))
      .attr("text-anchor", "middle")
      .style("font-size", "16px")
      .attr("fill", "red")
      .text("Bitcoin Chart");

    g.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x))
      .append("text")
      .attr("fill", "#000")
      .attr("y", -margin.bottom / 2)
      .attr("x", width)
      .attr('font-size', '20px')
      .attr("text-anchor", "end")
      .text("Date");

    g.append("g")
      .call(d3.axisLeft(y))
      .append("text")
      .attr("fill", "#000")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .attr('font-size', '20px')
      .attr("text-anchor", "end")
      .text("Price ($)");

    g.append("path")
      .datum(data)
      .attr("fill", "none")
      .attr("stroke", "red")
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("stroke-width", 1.5)
      .attr("d", line);
  }*/

}
