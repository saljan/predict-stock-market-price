import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
// import { AnnService } from "../../providers/ann.service";
import { AnnManagerService } from "../../providers/ann-manager/ann-manager.service";

@Component({
  selector: 'puz-app-tab-3',
  templateUrl: './tab-3.component.html',
  styleUrls: ['./tab-3.component.css']
})
export class Tab3Component implements OnInit {

  // ...
  formGroup: FormGroup;
  learningAlgo;
  algo_list = [
    { value: 'Back_Propagation', viewValue: 'Back Propagation simple', desc: 'small description ...' },
    // { value: '', viewValue: 'Back Propagation', desc: 'small description ...' },
    // { value: '', viewValue: 'Stochastic Gradient', desc: 'small description ...' }
  ];

  // ...
  constructor(private AnnManagerService: AnnManagerService) { }

  ngOnInit() {
    this.formGroup = this.initFormGroup();
    this.learningAlgo = this.AnnManagerService.getSelectedLearningAlgo()
  }

  // ...
  initFormGroup(): FormGroup {
    return new FormGroup({
      learning_algo: new FormControl(),
      learning_rate: new FormControl(),
      epoc: new FormControl(),
      error_tolerance: new FormControl(),
      iteration_interval: new FormControl()
    });
  }

  onSubmit(item){}
}
