import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../app/material.module";

// components ...
import { BtcChartD3Component } from "./btc-chart-d3/btc-chart-d3.component";
import { BtcChartComponent } from "./btc-chart/btc-chart.component";
import { Tab1Component } from "./tab-1/tab-1.component";
import { Tab2Component } from "./tab-2/tab-2.component";
import { Tab3Component } from "./tab-3/tab-3.component";
import { Tab4Component } from "./tab-4/tab-4.component";
import { Tab5Component } from "./tab-5/tab-5.component";


// providers ...
// import { SeoService } from "../../providers/seo.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    // BtcChartD3Component,
    BtcChartComponent,
    Tab1Component,
    Tab2Component,
    Tab3Component,
    Tab4Component,
    Tab5Component
  ],
  declarations: [
    BtcChartD3Component,
    BtcChartComponent,
    Tab1Component,
    Tab2Component,
    Tab3Component,
    Tab4Component,
    Tab5Component
  ],
  providers: []
})
export class ComponentsModule { }
