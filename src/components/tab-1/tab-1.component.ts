import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormGroup, FormControl } from "@angular/forms";
import { AnnService } from "../../providers/ann.service";
import { AnnManagerService } from "../../providers/ann-manager/ann-manager.service";
import { DataManagerService } from "../../providers/data-manager.service";
import { DataSet } from "../../model/data-set";

@Component({
  selector: 'puz-app-tab-1',
  templateUrl: './tab-1.component.html',
  styleUrls: ['./tab-1.component.css']
})
export class Tab1Component implements OnInit {

  // ...
  formGroup: FormGroup;
  DataSet: DataSet;
  stock_logo = [];


  // ...
  constructor(private AnnManagerService: AnnManagerService,
    private DataManagerService: DataManagerService) { }

  ngOnInit() {
    this.formGroup = this.initFormGroup();
    // this.DataSet = this.AnnService.getSelectedData();
    this.AnnManagerService.curentDataSet.subscribe(d => this.DataSet = d);
    this.stock_logo = this.DataManagerService.getStocks();
  }

  // ...
  initFormGroup(): FormGroup {
    return new FormGroup({
      stock: new FormControl(),
      start: new FormControl(),
      end: new FormControl(),
      price: new FormControl(),
      percenteg: new FormControl(),
      micro_date: new FormControl()
    });
  }

  onSubmit(item) {
    // this.AnnService.setSelectedData(item);
    item.start = typeof item.start === 'object' ? this.dateFormat(item.start) : item.start;
    item.end = typeof item.end === 'object' ? this.dateFormat(item.end) : item.end;
    this.AnnManagerService.setDataSet(item);
  }

  // ...
  LOG() {
    // console.log(this.AnnManagerService.getSelectedData());
    // console.log(this.AnnManagerService.data);
    console.log(this.DataSet);
    console.log(this.AnnManagerService.getDATA())
  }

  private dateFormat(date) {
    let mm = date.getMonth() + 1; // getMonth() is zero-based
    let dd = date.getDate();

    return [
      date.getFullYear(),
      '-' + (mm > 9 ? '' : '0') + mm,
      '-' + (dd > 9 ? '' : '0') + dd
    ].join('');
  }
}
