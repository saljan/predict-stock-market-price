import { Component, OnInit } from '@angular/core';
import { AnnManagerService } from "../../providers/ann-manager/ann-manager.service";
import { AnnDrawerService } from "../../providers/ann-drawer/ann-drawer.service";
import * as d3 from 'd3';
import { timer } from 'rxjs/observable/timer';
import { take, map } from "rxjs/operators";

@Component({
  selector: 'puz-app-tab-5',
  templateUrl: './tab-5.component.html',
  styleUrls: ['./tab-5.component.css']
})
export class Tab5Component implements OnInit {

  // ...
  ANN;
  epoc = 0;
  current_epoc = 0;
  interval: number = 1;
  stroke: number = 2;
  stop;
  start: boolean = true;
  margin = { left: 10, right: 10, top: 20, bottom: 20 };
  width = 500 - this.margin.left - this.margin.right;
  height = 500 - this.margin.top - this.margin.bottom;
  hide_static = true;
  min;
  max;
  mean;
  median;
  quantile25;
  quantile75;
  variance;
  deviation;


  // ...
  constructor(private AnnManagerService: AnnManagerService, private AnnDrawerService: AnnDrawerService) { }

  ngOnInit() {
    this.ANN = this.AnnManagerService.getSelectedANN();
  }

  // ...
  onClick() {
    let len = this.epoc = this.AnnManagerService.weights_for_simulation.length - 1;

    this.change_start();
    this.stop = timer(0, this.interval * 1000)
      .pipe(take(this.epoc), map(() => --this.epoc))
      .subscribe(d => {
        this.current_epoc = len - this.epoc;
        this.AnnDrawerService.draw_neurals(
          this.get_options(),
          this.ANN.input_num,
          this.ANN.layers_num.split(';').map(d => +d),
          this.ANN.output_num,
          this.AnnManagerService.weights_for_simulation[this.current_epoc]);
      }, (er) => console.log(er), () => this.change_start());
  }

  stop_proccess() {
    this.change_start();
    this.stop.unsubscribe();
  }

  onStatic() {
    let price = this.AnnManagerService.getDATA().dataset.price;
    // console.log(x);
    this.hide_static = false;
    this.min = this._min(price);
    this.max = this._max(price);
    this.mean = this._mean(price)
    this.median = this._median(price)
    this.quantile25 = this._quantile(price, .25);
    this.quantile75 = this._quantile(price, .75);
    this.variance = this._variance(price)
    this.deviation = this._deviation(price)
  }

  // private ...
  private get_svg() {
    document.getElementById('neural-svg').innerHTML = '';
    return d3.select('#neural-svg')
      .append('svg')
      .attr('class', 'neural')
      .attr('width', this.width)
      .attr('height', this.height);
  };

  private get_options() {
    return {
      svg: this.get_svg(),
      width: this.width,
      height: this.height,
      stroke: this.stroke
    }
  }

  private change_start() {
    this.start = !this.start;
  }

  _min(data) {
    return d3.min(data);
  }

  _max(data) {
    return d3.max(data);
  }

  _mean(data) {
    return d3.mean(data);
  }

  _median(data) {
    return d3.median(data);
  }
  _quantile(data, range) {
    return d3.quantile(data, range);
  }

  _variance(data) {
    return d3.variance(data);
  }

  _deviation(data) {
    return d3.deviation(data);
  }
}
