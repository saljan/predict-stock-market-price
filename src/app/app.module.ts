import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from "./material.module";
import { ProvidersModule } from "../providers/providers.module";
import { HomeModule } from "../pages/home/home.module";
import { LabModule } from "../pages/lab/lab.module";
import { Lab2Module } from "../pages/lab-2/lab-2.module";
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from "../environments/environment";


// ...
import { AppComponent } from './app.component';
import { SidenavComponent } from "../components/sidenav/sidenav.component";
import { NotFoundComponent } from "../pages/not-found/not-found.component";
import { SamplesComponent } from "../pages/samples/samples.component";

// ...
import { AdDirective } from "../directives/ad.directive";
import { AdBannerComponent } from "../components/ad-banner/ad-banner.component";


@NgModule({
  declarations: [
    AppComponent,
    AdDirective,
    AdBannerComponent,
    NotFoundComponent,
    SidenavComponent,
    SamplesComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HomeModule,
    LabModule,
    Lab2Module,
    ProvidersModule,
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.fire_config),
    AngularFireDatabaseModule
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
