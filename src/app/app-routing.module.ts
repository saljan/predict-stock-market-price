import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from "../pages/not-found/not-found.component";
import { SamplesComponent } from "../pages/samples/samples.component";

const routes: Routes = [
  // { path: 'regulamin', component: TermsComponent },
  { path: 'home', redirectTo: '', pathMatch: 'full' },
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: 'samples', component: SamplesComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
