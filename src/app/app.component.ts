import { Component } from '@angular/core';

@Component({
  selector: 'puz-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  // ...

  constructor() { }

  ngOnInit() {
  }

}
