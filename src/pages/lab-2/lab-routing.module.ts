import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// ...
import { Lab2Component } from "./lab-2.component";

const labRoutes: Routes = [
  {
    path: 'lab-2', component: Lab2Component,
    // data: { breadcrumb: "Home" },
    children: [
      // { path: '', component: AboutCardComponent } // some oferts to start
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(labRoutes)],
  exports: [RouterModule]
})
export class LabRoutingModule { }
