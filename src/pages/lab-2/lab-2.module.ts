import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../../app/material.module";
import { LabRoutingModule } from "./lab-routing.module";
import { ComponentsModule } from "../../components/components.module";


// ...
import { Lab2Component } from "./lab-2.component";
// import { Tab1Component } from "../../components/tab-1/tab-1.component";
// import { Tab2Component } from "../../components/tab-2/tab-2.component";
// import { Tab3Component } from "../../components/tab-3/tab-3.component";
// import { Tab4Component } from "../../components/tab-4/tab-4.component";
// import { BtcChartD3Component } from "../../components/btc-chart-d3/btc-chart-d3.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ComponentsModule,
    LabRoutingModule
  ],
  declarations: [
    Lab2Component,
    // BtcChartD3Component,  // import from component module
    // Tab1Component,
    // Tab2Component,
    // Tab3Component,
    // Tab4Component
  ]
})
export class Lab2Module { }
