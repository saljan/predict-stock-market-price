import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../../app/material.module";
import { LabRoutingModule } from "./lab-routing.module";
import { ComponentsModule } from "../../components/components.module";

// components ...
import { LabComponent } from "./lab.component";


// providers ...
// import { SeoService } from "../../providers/seo.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ComponentsModule,
    LabRoutingModule
  ],
  declarations: [
    LabComponent
  ],
  providers: []
})
export class LabModule { }
