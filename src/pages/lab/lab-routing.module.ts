import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// ...
import { LabComponent } from "./lab.component";

const labRoutes: Routes = [
  {
    path: 'lab', component: LabComponent,
    // data: { breadcrumb: "Home" },
    children: [
      // { path: '', component: AboutCardComponent } // some oferts to start
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(labRoutes)],
  exports: [RouterModule]
})
export class LabRoutingModule { }
