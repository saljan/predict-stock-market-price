import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AnnManagerService } from "../../providers/ann-manager/ann-manager.service";
import { ChartService } from "../../providers/chart/chart.service";
import { SettingsService } from "../../providers/settings/settings.service";
import { Observable } from "rxjs/Observable";
import * as Plotly from 'plotly.js';



@Component({
  selector: 'puz-app-lab',
  templateUrl: './lab.component.html',
  styleUrls: ['./lab.component.css']
})
export class LabComponent implements OnInit {

  // ...
  @ViewChild('learnERR') el_LR_ER: ElementRef;
  @ViewChild('testERR') el_TS_ER: ElementRef;
  @ViewChild('validateERR') el_VL_ER: ElementRef;
  @ViewChild('learnOut') el_LR_OUT: ElementRef;
  @ViewChild('testOut') el_TS_OUT: ElementRef;
  @ViewChild('validateOut') el_VL_OUT: ElementRef;
  EPOC_NUM: Observable<number>;
  start: boolean = true;
  reset: boolean = false;
  data_set_tab:Observable<boolean>;
  neural_tab:Observable<boolean>;
  simulation_tab:Observable<boolean>;

  // ...
  constructor(private AnnManagerService: AnnManagerService,
    private chartService: ChartService,
    private SettingsService: SettingsService) { }

  ngOnInit() {
    this.EPOC_NUM = this.AnnManagerService.current_EPOC_NUM;
    this.data_set_tab = this.SettingsService.data_set_tab;
    this.neural_tab = this.SettingsService.neural_tab;
    this.simulation_tab = this.SettingsService.simulation_tab;

    // this.AnnManagerService.curent_ERR_TS.subscribe(d => this.draw_errors(d, this.el_TS_ER, 'Error after TEST'));
    // this.AnnManagerService.curent_ANN_OUT_TS.subscribe(d => this.draw_output(d, this.el_TS_OUT));
  }

  // ...
  build_ann() {
    this.SettingsService.change_dataset_tab();
    this.SettingsService.change_neural_tab();
    this.AnnManagerService.buildANN();
    this.chartService.draw_errors('el_LR_ER', { MSE: [], RMSE: [], SSE: [] }, this.el_LR_ER);
    this.changeFlagReset();
  }

  //  LEARN
  learn_ann() {
    this.SettingsService.change_simulation_tab(false);
    this.AnnManagerService.learnANN({ out: this.el_LR_OUT, err: 'el_LR_ER' });
    this.changeFlagStart();
  }

  stop() {
    this.AnnManagerService.stop_proccess();
    this.changeFlagStart();
  }

  reset_func() {
    this.AnnManagerService.reset();
    this.changeFlagReset();
  }

  // TEST
  test_ann() {
    this.AnnManagerService.testANN({ out: this.el_TS_OUT, err: this.el_TS_ER });
  }

  // VALIDATE
  validate_ann() {
    this.AnnManagerService.validateANN();
  }

  draw_output(data, ele) {
    // console.log(data);
    if (data.out.length === 0) return;
    const element = ele.nativeElement;
    const DESIERD = {
      x: data.desired.map(d => d.date[0]),
      y: data.desired.map(d => d.price_norm[0]),
      name: 'desierd',
      line: {
        color: 'red'
      }
    };
    const ANN_OUT = {
      x: data.desired.map(d => d.date[0]),
      y: data.out[0],
      name: 'ann output',
      line: {
        color: 'blue'
      }
    };
    const formattedData = [DESIERD, ANN_OUT];

    const style = {
      margin: { t: 60 },
      // width: 500,
      // height: 500,
      title: 'ANN output',
      xaxis: { title: 'Date' },
      yaxis: { title: 'Price ($)' }
    }

    Plotly.newPlot(element, formattedData, style)
  }

  draw_errors(data, ele, title = 'Errors after learn') {
    // console.log(data);
    if (data.MSE.length === 0) return;
    const element = ele.nativeElement;
    const MSE = {
      y: data.MSE,
      name: 'MSE',
      line: {
        color: 'red'
      }
    };
    const RMSE = {
      y: data.RMSE,
      name: 'RMSE',
      line: {
        color: 'blue'
      }
    };
    const SSE = {
      y: data.SSE,
      name: 'SSE',
      line: {
        color: 'orange'
      }
    };
    const formattedData = [MSE, RMSE, SSE];

    const style = {
      margin: { t: 60 },
      width: 500,
      height: 500,
      title,
      xaxis: { title: 'EPOCS' }
      // yaxis: { title: 'Price ($)' }
    }

    Plotly.newPlot(element, formattedData, style)
  }

  // ...
  changeFlagStart() {
    this.start = !this.start;
  }

  changeFlagReset() {
    this.reset = !this.reset;
  }
}