import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';
import { MaterialModule } from "../../app/material.module";
import { HomeRoutingModule } from './home-routing.module';

// components ...
import { HomeComponent } from "./home.component";

// providers ...
// import { SeoService } from "../../providers/seo.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent
  ],
  providers: []
})
export class HomeModule { }
