import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// ...
import { HomeComponent } from "./home.component";

const homeRoutes: Routes = [
  {
    path: '', component: HomeComponent,
    // data: { breadcrumb: "Home" },
    children: [
      // { path: '', component: AboutCardComponent } // some oferts to start
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
