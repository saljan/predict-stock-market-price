import { Component, OnInit } from '@angular/core';
import { DataManagerService } from "../../providers/data-manager.service";
import { SeoService } from "../../providers/seo.service";
import { ManageANN } from "../../core/manage-ann";
import { Chart } from "chart.js";
// import * as d3 from "d3";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // ...
  ManageANN: ManageANN = new ManageANN();


  // ...
  constructor(private SeoService: SeoService, private DataManagerService: DataManagerService) { }

  ngOnInit(): void {
    // this.SeoService.setTagsFor('home');

    let x = [
      { 'x1': 0, 'x2': 0 },
      { 'x1': 1, 'x2': 0 },
      { 'x1': 0, 'x2': 1 },
      { 'x1': 1, 'x2': 1 }
    ];

    let y = [
      { 'd': 0 },
      { 'd': 1 },
      { 'd': 1 },
      { 'd': 0 }
    ];

    // this.ManageANN.build_ann([2, 3, 2, 1]);
    // for (var index = 0; index < 10; index++) {
    //   this.ManageANN.learn_ann('S_SUCH_GRAD', x, y);
    // }

    // console.log(this.ManageANN.get_err_per_EPO().SE)
    

    // this.ManageANN.build_ann();   
  }
}
