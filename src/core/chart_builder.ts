// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
/**
 * @author :Salem Albarudy.
 * @license :MIT.
 * @version :v.0.1.2
 */
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
import * as d3 from "d3";

// ...
let options = {
    selector: '',
    margin: { top: 20, right: 20, bottom: 50, left: 40 },
    width: 700,
    height: 500,
    svg: null,
    g: null,
    chart_title: {
        title: 'chart_title',
        font_size: 16,
        fill: 'red'
    },
    xLabel: {
        title: 'xLabel',
        font_size: 16,
        fill: 'red'
    },
    yLabel: {
        title: 'xLabel',
        font_size: 16,
        fill: 'red'
    }
}

// let isBuilt = false,
let data = [],
    xVal = [],
    yVal = [];

// ...
let init_options = (opt) => {
    // isBuilt = true;
    options = {
        selector: '',
        margin: { top: 20, right: 20, bottom: 50, left: 40 },
        width: 700,
        height: 500,
        svg: null,
        g: null,
        chart_title: {
            title: 'chart_title',
            font_size: 16,
            fill: 'red'
        },
        xLabel: {
            title: 'xLabel',
            font_size: 16,
            fill: 'red'
        },
        yLabel: {
            title: 'xLabel',
            font_size: 16,
            fill: 'red'
        },

        ...opt
    };
};

let add_data = (d, xKey, yKey) => {
    data = d;
    xVal = d.map(dd => dd[xKey]);
    yVal = d.map(dd => dd[yKey]);
};

let add_svg = () => {
    // ...
    d3.select(options.selector).html('');

    // ...
    options.svg = d3.select(options.selector)
        .append('svg')
        .attr('width', options.width + options.margin.left + options.margin.right)
        .attr('height', options.height + options.margin.top + options.margin.bottom)
        .attr('class', 'btc-svg');  /// ?????
    options.g = options.svg.append("g")
        .attr("transform", "translate(" + options.margin.left + "," + options.margin.top + ")");

    // Title ...
    options.g.append("text")
        .attr("x", (options.width / 2))
        .attr("y", (options.margin.top / 2))
        .attr("text-anchor", "middle")
        .style("font-size", `${options.chart_title.font_size}px`)
        .attr("fill", options.chart_title.fill)
        .text(options.chart_title.title);

}

let add_xAxis = () => {
    // xAxis ...
    options.g.append("g")
        .attr("transform", "translate(0," + options.height + ")")
        .call(d3.axisBottom(options['xScale']))
        .append("text")
        .attr("fill", options.xLabel.fill)
        .attr("y", -options.margin.bottom / 2)
        .attr("x", options.width)
        .attr('font-size', `${options.xLabel.font_size}px`)
        .attr("text-anchor", "end")
        .text(options.xLabel.title);
};

let add_yAxis = () => {
    options.g.append("g")
        .call(d3.axisLeft(options['yScale']))
        .append("text")
        .attr("fill", options.yLabel.fill)
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .attr('font-size', `${options.yLabel.font_size}px`)
        .attr("text-anchor", "end")
        .text(options.yLabel.title);
};

let add_xScale = (type) => {
    switch (type) {
        case 'LINEAR':
            options['xScale'] = scaleLinear(xVal, [0, options.width]);
            break;
        case 'TIME':
            options['xScale'] = scaleTime(xVal, [0, options.width]);
            break;
        default:
            break;
    }
};

let add_yScale = (type) => {
    switch (type) {
        case 'LINEAR':
            options['yScale'] = scaleLinear(yVal, [options.height, 0]);
            break;
        case 'TIME':
            options['yScale'] = scaleTime(yVal, [options.height, 0]);
            break;
        default:
            break;
    }
};

let draw_line = (keys, d = data, color = 'black') => {
    let line = d3.line()
        .x((dd) => options['xScale'](dd[keys[0]]))
        .y((dd) => options['yScale'](dd[keys[1]]));

    options.g.append("path")
        .datum(d)
        .attr("fill", "none")
        .attr("stroke", color)
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round")
        .attr("stroke-width", 1.5)
        .attr("d", line);
};

// private ...
let init_svg = (data, options) => {
    // ...
    d3.select(options.selector).html('');

    // ...
    options.svg = d3.select(options.selector)
        .append('svg')
        .attr('width', options.width + options.margin.left + options.margin.right)
        .attr('height', options.height + options.margin.top + options.margin.bottom)
        .attr('class', 'btc-svg');  /// ?????
    options.g = options.svg.append("g")
        .attr("transform", "translate(" + options.margin.left + "," + options.margin.top + ")");

    // Title ...
    options.g.append("text")
        .attr("x", (options.width / 2))
        .attr("y", (options.margin.top / 2))
        .attr("text-anchor", "middle")
        .style("font-size", `${options.chart_title.font_size}px`)
        .attr("fill", options.chart_title.fill)
        .text(options.chart_title.title);

    // xAxis ...
    options.g.append("g")
        .attr("transform", "translate(0," + options.height + ")")
        .call(d3.axisBottom(options.xScale))
        .append("text")
        .attr("fill", options.xLabel.fill)
        .attr("y", -options.margin.bottom / 2)
        .attr("x", options.width)
        .attr('font-size', `${options.xLabel.font_size}px`)
        .attr("text-anchor", "end")
        .text(options.xLabel.title);

    // yAxis ...
    options.g.append("g")
        .call(d3.axisLeft(options.yScale))
        .append("text")
        .attr("fill", options.yLabel.fill)
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .attr('font-size', `${options.yLabel.font_size}px`)
        .attr("text-anchor", "end")
        .text(options.yLabel.title);
};

let scaleLinear = (data, range) => {
    return d3.scaleLinear().domain([+d3.min(data), +d3.max(data)]).rangeRound(range);
};

let scaleTime = (date = [], range) => {
    return d3.scaleTime().domain(d3.extent(date)).rangeRound(range);
};

// GET/SET ...
let set_options = (opt) => options = opt;
let get_options = () => options;

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //

export class ChartBuilder {
    // keep the order...
    init_options = init_options;
    add_data = add_data;
    add_svg = add_svg;
    add_xScale = add_xScale;
    add_yScale = add_yScale;
    add_xAxis = add_xAxis;
    add_yAxis = add_yAxis;
    draw_line = draw_line;


    // GET/SET ...
    set_options = set_options;
    get_options = get_options;
}
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //