// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
/**
 * @author :Salem Albarudy.
 * @license :MIT.
 * @version :v.0.1.2
 */
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
import { FactorANN } from "./ann";
import { Simple_Stochastic_Gradient } from "./simple_stochastic_gradient";

// ...
const ANN = new FactorANN(),
    SIMPLE_SOCH_GRAD = new Simple_Stochastic_Gradient();

let LEARNIG_RATE = 0.1,
    ERROR_PER_EPOC = { leran_algo: '', EPO_NUM: 0, SE: [], MSE: [] },
    TMP_TESTING_OUT = { leran_algo: '', EPO_NUM: 0, SE: [], MSE: [] },
    TMP_VALIDATE_OUT = { leran_algo: '', EPO_NUM: 0, SE: [], MSE: [] };


// ...
let build_ann = (neu_arr = [1, 1], bias = true, actv = 'sigmu') => {
    ANN.set_activation_func(actv);
    ANN.set_bias(bias);
    ANN.set_layers_num(neu_arr);


    ANN.get_all_info();  // <-- <-- <---
};

let learn_ann = (data = [], y = [], leran_algo = 'Back_Propagation', lr = LEARNIG_RATE, for_results = ERROR_PER_EPOC) => {
    switch (leran_algo) {
        case 'Back_Propagation':
            ERROR_PER_EPOC = SIMPLE_SOCH_GRAD.learn_ann_scht_grad(data, y, ANN, lr, ERROR_PER_EPOC);
            break;
        case 'COM_SUCH_GRAD':
            // SIMPLE_SOCH_GRAD.learn_ann_scht_grad(data, y, ANN, lr);
            break;
        case 'PATCH_GRAD':
            // SIMPLE_SOCH_GRAD.learn_ann_scht_grad(data, y, ANN, lr);
            break;
        default:
            break;
    }

};


/*let test_ann = (START_LEARN = false, data = [], y = [], leran_algo = 'S_SUCH_GRAD', lr = LEARNIG_RATE) => {
    if (!START_LEARN) {
        TMP_TESTING_OUT = SIMPLE_SOCH_GRAD.test_ann(data, y, ANN, LEARNIG_RATE);
    } else {
        TMP_TESTING_OUT = SIMPLE_SOCH_GRAD.learn_ann_scht_grad(data, y, ANN, lr, TMP_TESTING_OUT);
        // learn_ann(data, y, leran_algo, lr, TMP_TESTING_OUT);
    }
}*/

let test_ann = (data = [], y = [], leran_algo = 'S_SUCH_GRAD') => {
    TMP_TESTING_OUT = SIMPLE_SOCH_GRAD.test_ann(data, y, ANN);
};

let validate_ann = (START_LEARN = false, data = [], y = [], leran_algo = 'S_SUCH_GRAD', lr = LEARNIG_RATE) => {
    if (!START_LEARN) {
        TMP_VALIDATE_OUT = SIMPLE_SOCH_GRAD.validate_ann(data, y, ANN, LEARNIG_RATE);
    } else {
        TMP_VALIDATE_OUT = SIMPLE_SOCH_GRAD.learn_ann_scht_grad(data, y, ANN, lr, TMP_VALIDATE_OUT);
        // learn_ann(data, y, leran_algo, lr, TMP_VALIDATE_OUT);
    }
}

// ...
let get_err_per_EPO = () => ERROR_PER_EPOC;

let get_test_err_per_EPO = () => TMP_TESTING_OUT;

let get_validate_err_per_EPO = () => TMP_VALIDATE_OUT;

let get_all_MSE = () => {
    let err = get_err_per_EPO();
    err.MSE = [];

    for (let i = 0; i < err.EPO_NUM; i++) {
        err.MSE.push(SIMPLE_SOCH_GRAD.MSE(err[`epo_error_${i}`]));
    }

    return err.MSE;
};

let get_all_RMSE = () => {
    let err = get_err_per_EPO();
    err['RMSE'] = [];

    for (let i = 0; i < err.EPO_NUM; i++) {
        err['RMSE'].push(SIMPLE_SOCH_GRAD.RMSE(err[`epo_error_${i}`]));
    }

    return err['RMSE'];
};

let get_all_SSE = () => {
    let err = get_err_per_EPO();
    err.SE = [];

    for (let i = 0; i < err.EPO_NUM; i++) {
        err.SE.push(SIMPLE_SOCH_GRAD.SSE(err[`epo_error_${i}`]));
    }

    return err.SE;
};

// LEARNING / TESTING / VALIDATION ...
let get_MSE_FOR = (arg = 'LEARNING') => {
    switch (arg) {
        case 'LEARNING':
            return get_MSE(get_err_per_EPO());
        case 'TESTING':
            return get_MSE(get_test_err_per_EPO());
        case 'VALIDATION':
            return get_MSE(get_validate_err_per_EPO());
        default:
            break;
    }
};

// let get_MSE_FOR = (arg = 'LEARNING') =>
//     [{ key: 'LEARNING', data_ref: get_err_per_EPO }, // check if work with func ref just
//     { key: 'TESTING', data_ref: get_test_err_per_EPO },
//     { key: 'VALIDATION', data_ref: get_validate_err_per_EPO }
//     ].filter(d => {
//         if (d.key === arg){
//             console.log(d.key)  // !!! 
//             console.log(d.data_ref()); // !!! 
//             return get_MSE(d.data_ref());
//         }
//     });

// LEARNING / TESTING / VALIDATION ...
let get_RMSE_FOR = (arg = 'LEARNING') => {
    switch (arg) {
        case 'LEARNING':
            return get_RMSE(get_err_per_EPO());
        case 'TESTING':
            return get_RMSE(get_test_err_per_EPO());
        case 'VALIDATION':
            return get_RMSE(get_validate_err_per_EPO());
        default:
            break;
    }
};

// LEARNING / TESTING / VALIDATION ...
let get_SSE_FOR = (arg = 'LEARNING') => {
    switch (arg) {
        case 'LEARNING':
            return get_SSE(get_err_per_EPO());
        case 'TESTING':
            return get_SSE(get_test_err_per_EPO());
        case 'VALIDATION':
            return get_SSE(get_validate_err_per_EPO());
        default:
            break;
    }
};

let get_MSE = (err: any = {}) => SIMPLE_SOCH_GRAD.MSE(err[`epo_error_${err.EPO_NUM - 1}`]);

let get_RMSE = (err: any = {}) => SIMPLE_SOCH_GRAD.RMSE(err[`epo_error_${err.EPO_NUM - 1}`]);

let get_SSE = (err: any = {}) => SIMPLE_SOCH_GRAD.SSE(err[`epo_error_${err.EPO_NUM - 1}`]);

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
export class API {
    // ...
    get_ann = (): FactorANN => ANN;
    get_err_per_EPO = get_err_per_EPO;
    get_test_err_per_EPO = get_test_err_per_EPO;
    get_validate_err_per_EPO = get_validate_err_per_EPO;

    get_MSE_FOR = get_MSE_FOR;
    get_RMSE_FOR = get_RMSE_FOR;
    get_SSE_FOR = get_SSE_FOR;
    get_all_MSE = get_all_MSE;
    get_all_RMSE = get_all_RMSE;
    get_all_SSE = get_all_SSE;
    get_MSE = get_MSE;
    get_RMSE = get_RMSE;
    get_SSE = get_SSE;

    setMSE = (mse) => get_err_per_EPO().MSE.push(mse);
    setRMSE = (rmse) => get_err_per_EPO()['RMSE'].push(rmse);
    setSSE = (sse) => get_err_per_EPO().SE.push(sse);

    // ...
    build_ann = build_ann;

    learn_ann = learn_ann;
    test_ann = test_ann;
    validate_ann = validate_ann;
}
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //