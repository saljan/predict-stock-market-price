// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
/**
 * @author :Salem Albarudy.
 * @license :MIT.
 * @version :v.0.1.2
 */
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
import * as math from "mathjs";


// ...
let ERROR_PER_EPO = { leran_algo: '', EPO_NUM: 0, SE: [], MSE: [] },
    TMP_TESTING_OUT = { leran_algo: '', EPO_NUM: 0, SE: [], MSE: [] },
    TMP_VALIDATE_OUT = { leran_algo: '', EPO_NUM: 0, SE: [], MSE: [] };
    // W_PER_EPO = [{EPO_NUM: 0, }];


// ...
let forward = (x, ANN) => {
    for (let i = 0; i < ANN.get_weight()['w_num']; i++) {
        if (i == 0) {
            ANN.get_output()[`out_${i}`] = ANN.net_output(ANN.get_weight()[`w_${i}`], x);
        } else {
            ANN.get_output()[`out_${i}`] = ANN.net_output(ANN.get_weight()[`w_${i}`], ANN.get_output()[`out_${i - 1}`]);
        }
    }
};


let error_signal = (y, ANN) => {
    // ANN.error_signal(y);
    for (let i = ANN.get_output()['out_num'] - 1; i >= 0; i--) {
        if (i == ANN.get_output()['out_num'] - 1) {
            ANN.get_error()[`error_${i}`] = math.subtract(y, ANN.get_output()[`out_${i}`]);
        } else {
            let tmp = [...ANN.get_weight()[`w_${i + 1}`]];
            if (ANN.isBias()) tmp.shift();// remove the bias row; it's not output. just in.
            ANN.get_error()[`error_${i}`] = math.multiply(tmp, ANN.get_error()[`error_${i + 1}`]);
        }
    }
};

// ...
// wi^ = wi + lr * eri * (dfi(e)/de) * xi; 
// (dfi(e)/de) is derivative of actv. func. feeded with out. of neu.
// ex. sigmu^ ->  sigmu * (1 - sigmu). --> sigmu(out[i]) * (1 - sigmu(out[i]));
// ...
// rember to convert x. or out[i-1] --> [x]. [out[i-1]] // math.multiply() need this other wies 
// we will get vector not array and we need array for it.
let backpropagation = (x, lr, ANN) => {
    for (let i = 0; i < ANN.get_lay()['lay_num']; i++) {
        let ers = [...ANN.get_error()[`error_${i}`]],
            outs = [...ANN.get_output()[`out_${i}`]],
            de = [], adj = [];

        // OPTIMIZE ...
        // ANN.isBias() && (i != ANN.get_lay()['lay_num'] - 1)
        if (ANN.isBias()) {
            x.unshift(1);
            if ((i != ANN.get_lay()['lay_num'] - 1)) {
                outs.shift();
            }
        }

        // de = outs.map((out, idx) => lr * ers[idx] * ANN.sigmu_derivative(out)); // we add new derivative funcs.
        de = outs.map((out, idx) => lr * ers[idx] * ANN.derivative(out, ANN.get_actv_func()));
        if (i != 0) {
            adj = math.transpose(math.multiply(de.map(x_ => [x_]), [ANN.get_output()[`out_${i - 1}`]]));
        } else {
            adj = math.transpose(math.multiply(de.map(x_ => [x_]), [x]));
        }
        ANN.set_weight_for(i, math.add(ANN.get_weight()[`w_${i}`], adj));
    }
};

// ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... //
//  Simpl suchastic gradint 
// param :
// 1. data:[], data for learn/test/validation;
// 2. y:[], desierd output;
// 3. EPO_NUM:number; number of EPOC.
// case : learing 
// 1. pass data/y to learn in array;
// 2. loop through the data array
// 3. each iteration DO -->
//    3.1. forword;
//    3.2. error signal;
//    3.3. backpropagation;
//    3.4. last error signal add to >> errors --> ERR_EPO:['EPO_NUM':{errors, SSE}];
// 4. at the end of iteration DO -->
//    4.1. count SSE and add to >> SSE --> ERR_EPO:['EPO_NUM':{errors, SSE}];
//    4.2. SE = 0.5 * SUM (target[i] - net_out[i] ); i -->  every iteration;
let learn_ann_scht_grad = (data, y, ANN, LEARNIG_RATE, for_results) => {  // #--> #-->  LEARNIG_RATE <--# <--# 
    for (let i = 0; i < data.length; i++) {
        forward(Object.values(data[i]), ANN); // Second Arg is defined ANN
        error_signal(Object.values(y[i]), ANN);  // second Arg is defined ANN
        backpropagation(Object.values(data[i]), LEARNIG_RATE, ANN); // second Arg is defined ANN
        manage_error(    // <--- <--- FIND SOL.
            ANN.get_error()[`error_${ANN.get_error()['error_num'] - 1}`],
            ANN.get_output()[`out_${ANN.get_output()['out_num'] - 1}`],
            for_results
        );
    }
    //** squared_error(for_results); 
    //** mean_squared_error(for_results);
    for_results.EPO_NUM += 1;

    return for_results;
};

// .... FOR ONE OUTPUT NEU. UPDATE !!!  ... ///
let manage_error = (error, output, for_results) => {
    if (for_results[`epo_error_${for_results.EPO_NUM}`] === undefined) {
        for_results[`epo_error_${for_results.EPO_NUM}`] = [];
        for_results[`epo_out_${for_results.EPO_NUM}`] = [];
    }
    for_results[`epo_error_${for_results.EPO_NUM}`].push(error);
    for_results[`epo_out_${for_results.EPO_NUM}`].push(output);
};

// SE (Squared Error) 
// SE = 0.5 * SUM[(target[i] - net_out[i] )^2];
let squared_error = (for_results) => for_results[`SE`].push(.5 * math.sum(math.subtract(get_out_and_error(for_results)[0], get_out_and_error(for_results)[1])[0].map(d => math.pow(d, 2))));

// MSE (Mean Squared Error)
let mean_squared_error = (for_results) => for_results[`MSE`].push(math.sum(math.subtract(get_out_and_error(for_results)[0], get_out_and_error(for_results)[1])[0].map(d => math.pow(d, 2))) / get_out_and_error(for_results)[0].length);

// JUST ONE NEU.
// MSE = SUM[ ( d - y )^2 ] / n
let MSE = (err) => math.sum(math.transpose(err)[0].map(n => math.pow(n, 2))) / math.transpose(err)[0].length;

// RMSE = sqrt(MSE)
let RMSE = (err) => math.sqrt(MSE(err));

let SSE = (err) => .5 * math.sum(math.transpose(err)[0].map(n => math.pow(n, 2)));

let get_out_and_error = (for_results) => {
    return [
        math.transpose(for_results[`epo_out_${for_results.EPO_NUM}`]),
        math.transpose(for_results[`epo_error_${for_results.EPO_NUM}`])
    ];
};


// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
// TEST 
// # 1. here will be one EPO to check the ANN just 
// 1.1. We send array of testing data. and desierd data.
// 1.2. pass each row to ANN we get output
// 1.3. every out of each row we tak and add to TMP_ARRAY_OUT
// 1.4. TMP_ARRAY_OUT has the output and comper with desierd out for MSE SSE.
// 1.5. ddraw the TMP_ARRAY_OUT and the MSE SEE

// # 2. user deside how many EPO after visiualistaion TMP_ARRAY_OUT and MSE 
// 2.1. start testing 
// 2.2. send the data test and disierd to learn because it is the same process but with new data. 
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //

let test_ann = (data, y, ANN) => {
    for (let i = 0; i < data.length; i++) {
        forward(Object.values(data[i]), ANN); // Sedcond Arg is defined ANN
        error_signal(Object.values(y[i]), ANN);  // Sedcond Arg is defined ANN
        // we just testing not fixing        // backpropagation(Object.values(data[i]), LEARNIG_RATE, ANN); // Sedcond Arg is defined ANN
        manage_error(    // <--- <--- FIND SOL.
            ANN.get_error()[`error_${ANN.get_error()['error_num'] - 1}`],
            ANN.get_output()[`out_${ANN.get_output()['out_num'] - 1}`],
            TMP_TESTING_OUT
        );
    }
    // squared_error(TMP_TESTING_OUT);
    // mean_squared_error(TMP_TESTING_OUT);
    TMP_TESTING_OUT.EPO_NUM += 1;

    return TMP_TESTING_OUT;
}


let validate_ann = (data, y, ANN, LEARNIG_RATE) => {
    for (let i = 0; i < data.length; i++) {
        forward(Object.values(data[i]), ANN); // Sedcond Arg is defined ANN
        error_signal(Object.values(y[i]), ANN);  // Sedcond Arg is defined ANN
        // we just validate not fixing        // backpropagation(Object.values(data[i]), LEARNIG_RATE, ANN); // Sedcond Arg is defined ANN
        manage_error(    // <--- <--- FIND SOL.
            ANN.get_error()[`error_${ANN.get_error()['error_num'] - 1}`],
            ANN.get_output()[`out_${ANN.get_output()['out_num'] - 1}`],
            TMP_VALIDATE_OUT
        );
    }
    squared_error(TMP_VALIDATE_OUT);
    mean_squared_error(TMP_VALIDATE_OUT);
    TMP_VALIDATE_OUT.EPO_NUM += 1;

    return TMP_VALIDATE_OUT;
}


// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
export class Simple_Stochastic_Gradient {
    // ...
    // get_err_per_EPO = () => ERROR_PER_EPO;
    MSE = MSE;
    RMSE = RMSE;
    SSE = SSE;

    learn_ann_scht_grad = learn_ann_scht_grad;
    test_ann = test_ann;
    validate_ann = validate_ann;
}
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //