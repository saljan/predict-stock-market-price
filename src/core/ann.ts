// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
/**
 * @author :Salem Albarudy.
 * @license :MIT.
 * @version :v.0.1.2
 */
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
import * as math from "mathjs";


// ...
const BIAS = 1;
let actv_func = 'sigmu',
    bias = true,
    in_num = 0,
    out_num = 0,
    lay_num = 0,
    lay_info = {},
    info_w = {},
    out_info = {},
    error_info = {},

    all_info = {
        bias,
        in_num,
        lay_num,
        out_num,
        info_w
    };


// ...
let set_activation_func = (actv = 'sigmu') => actv_func = actv;
let set_bias = (b = true) => bias = b;

// ..
// def. in_num = 0, b f, num 0  ->  --> num <= 0 in_num+= 1;
// def. in_num = 0, b f, num 1  ->  --> num >  0 in_num+= num;
//
// def. in_num = 0, b t, num 0  ->  in_num+= num+1;
// def. in_num = 0, b t, num 1  ->  in_num+= num+1;
//
// def. in_num = 0, b f, num 2  ->  in_num+= num;
// def. in_num = 0, b t, num 2  ->  in_num+= num+1;
// ...
//let set_input_num = (num = 0) => in_num += bias ? num + 1 : num > 0 ? num : 1;

// ...
//let set_output_num = (num = 1) => out_num += num;

// ...
// A.
// def. lay_num = 1, neus = [] -> lay_num+= neus.len;
// def. lay_num = 1, neus = [3,2,4,...]  -> lay_num+= neus.len;
// B. 
// 
let set_layers_num = (neus = [1, 1]) => {
    // ...
    lay_info = { lay_num: lay_num += neus.length - 1 };
    neus.map((neu, i) => {
        if (i != neus.length - 1)
            lay_info[`lay_${i}`] = { in: neus[i] + (bias ? 1 : 0), out: neus[i + 1] };
    });

    // ...
    in_num = get_lay()['lay_0'].in;
    out_num = get_lay()['lay_' + (get_lay()['lay_num'] - 1)].out;

    // ...
    init_weights();
    init_output();
    init_error();
};

// ...
// w [ in, out] 
// -- b. t -> in +1; b. f in +0;
// -- each col -> is one neu. == out; 
// -- each row in one col -> input data == in;
// -- first row of array if bias true -> def. == 1;
// 
// ex. b. f.  w[3, 2], all w are rand.
// | w1  w4 |
// | w2  w5 |
// | w3  w6 |
//
// ex. b. t.  w[3+1, 2], w1 = w5 = BIAS = 1; rest w are rand.
// | w1 = b  w5 = b |
// | w2      w6     |
// | w3      w7     |
// | w4      w8     |

let init_weights = () => {
    info_w = { w_num: lay_info['lay_num'] };
    for (let i = 0; i < lay_info['lay_num']; i++) {
        info_w[`w_${i}`] = math.random([lay_info[`lay_${i}`].in, lay_info[`lay_${i}`].out]);
    }
};

// private ...
let init_output = () => {
    out_info = { out_num: lay_info['lay_num'] };
    for (let i = 0; i < lay_info['lay_num']; i++) {
        out_info[`out_${i}`] = math.zeros([lay_info[`lay_${i}`].out]);
    }
};

// private ...
let init_error = () => {
    error_info = { error_num: lay_info['lay_num'] };
    for (let i = 0; i < lay_info['lay_num']; i++) {
        error_info[`error_${i}`] = math.zeros([lay_info[`lay_${i}`].out]);
    }
};

// ...
// ADD TD.  .) remove transpose I think send directly trans. w.
// ...
// w -> w^T
// x -> x
// u = w^T * x;
let net_input = (w, x) => {
    // OPTMIZE ...
    bias ? x.unshift(BIAS) : 0;
    return math.multiply(math.transpose(w), x);
};

// ...
// u = w^T * x;
let net_output = (w, x, actv = actv_func) => net_input(w, x).map(u => activation(u, actv));

let activation = (x = 0, type = 'sigmu') => {
    // console.log(`actvation function ${type}`);
    switch (type) {
        case 'sigmu':
            return sigmu_actv(x);
        case 'cos':
            return cos_actv(x);
        case 'tanh':
            return tanh_actv(x);
        case 'linear':
            return linear_actv(x);
        default:
            return 0;
    }
}

let derivative = (x = 0, type = 'sigmu') => {
    // console.log(`actvation function ${type}`);
    switch (type) {
        case 'sigmu':
            return sigmu_derivative(x);
        case 'cos':
            return cos_derivative(x);
        case 'tanh':
            return tanh_derivative(x);
        default:
            return 0;
    }
}

let linear_actv = (x) => x >= 0 ? 1 : 0;

let cos_actv = (x) => math.cos(x);

let tanh_actv = (x) => math.tanh(x);

let cos_sigmu_actv = (x) => math.cos(1 / (1 + math.exp(-x)));

let sigmu_actv = (x) => 1 / (1 + math.exp(-x));

let sigmu_derivative = (x) => sigmu_actv(x) * (1 - sigmu_actv(x));

let cos_derivative = (x) => -1. * math.sqrt(1 - math.pow(cos_actv(x), 2));

let tanh_derivative = (x) => 1 - math.pow(tanh_actv(x), 2);


// SET ...
let set_weight = (w) => info_w = w;
let set_weight_for = (i, w) => info_w[`w_${i}`] = w;

// GET ...
let get_lay = () => lay_info;
let get_weight = () => info_w;
let get_output = () => out_info;
let get_error = () => error_info;

// INFO ...
let get_info_for = (name, data) => {
    console.log(`---------------------------------------------------------------------------------`);
    console.log(`INFO ${name} array :`);
    console.log(`--------------------`);
    console.log(data);
    console.log(`---------------------------------------------------------------------------------`);
};

let get_weight_info = () => {
    get_info_for('weight', get_weight());
    // for (let i = 0; i < info_w.w_num; i++) {
    //     console.log(info_w['w-' + i]);
    // }
};

let get_lay_info = () => {
    get_info_for('layers', get_lay());
    // for (let i = 0; i < lay_info['lay_num']; i++) {
    //     console.log(lay_info['lay-' + i]);
    // }
};

let get_out_info = () => {
    get_info_for('output', get_output());
};

let get_error_info = () => {
    get_info_for('error', get_error());
};



let get_all_info = () => {
    console.log(`---------------------------------------------------------------------------------`);
    console.log(`INFO ANN :`);
    console.log(`--------------------`);
    console.log(`Input number : ${in_num}`);
    console.log(`Output number : ${out_num}`);
    console.log(`Layrs number : ${lay_num}`);
    console.log(`Bias : ${bias}`);
    console.log(`Activation function : ${actv_func}`);
    get_weight_info();
    get_lay_info();
    get_error_info();
    get_out_info();
    console.log(`---------------------------------------------------------------------------------`);
};

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
export class FactorANN {

    // ...

    // ...
    set_activation_func = set_activation_func;
    set_bias = set_bias;
    // set_input_num= set_input_num;
    // set_output_num= set_output_num;
    set_layers_num = set_layers_num;
    set_weight = set_weight;
    set_weight_for = set_weight_for;    


    // ...
    activation = activation;
    derivative = derivative;
    net_output = net_output;
    // error_signal= error_signal;
    sigmu_derivative = sigmu_derivative;

    // ...
    isBias = () => bias;
    get_lay = get_lay;
    get_weight = get_weight;
    get_output = get_output;
    get_error = get_error;


    // ...
    get_actv_func = () => actv_func;
    get_weight_info = get_weight_info;
    get_lay_info = get_lay_info;
    get_all_info = get_all_info;

};

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //