// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
/**
 * @author :Salem Albarudy.
 * @license :MIT.
 * @version :v.0.1.2
 */
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
import { FactorANN } from "./ann";
import * as math from "mathjs";

// ...
const ANN = new FactorANN();

let LEARNIG_RATE = 0.1,
    // err_EPO = [],
    // learning_data = [],
    // testing_data = [],
    // validation_data = [],
    // learn_output = [],
    // test_output = [],
    // valid_output = [],
    ERROR_PER_EPO = { leran_algo: '', EPO_NUM: 0, SE: [], MSE: [] };

// ...
let build_ann = (neu_arr = [1, 1], bias = true, actv = 'sigmu') => {
    ANN.set_activation_func(actv);
    ANN.set_bias(bias);
    ANN.set_layers_num(neu_arr);


    ANN.get_all_info();
};


// ...
let forward = (x) => {
    for (let i = 0; i < ANN.get_weight()['w_num']; i++) {
        if (i == 0) {
            ANN.get_output()[`out_${i}`] = ANN.net_output(ANN.get_weight()[`w_${i}`], x);
        } else {
            ANN.get_output()[`out_${i}`] = ANN.net_output(ANN.get_weight()[`w_${i}`], ANN.get_output()[`out_${i - 1}`]);
        }
    }
};

let error_signal = (y) => {
    // ANN.error_signal(y);
    for (let i = ANN.get_output()['out_num'] - 1; i >= 0; i--) {
        if (i == ANN.get_output()['out_num'] - 1) {
            ANN.get_error()[`error_${i}`] = math.subtract(y, ANN.get_output()[`out_${i}`]);
        } else {
            let tmp = [...ANN.get_weight()[`w_${i + 1}`]];
            if (ANN.isBias()) tmp.shift();// remove the bias row; it's not output. just in.
            ANN.get_error()[`error_${i}`] = math.multiply(tmp, ANN.get_error()[`error_${i + 1}`]);
        }
    }
};

// ...
// wi^ = wi + lr * eri * (dfi(e)/de) * xi; 
// (dfi(e)/de) is derivative of actv. func. feeded with out. of neu.
// ex. sigmu^ ->  sigmu * (1 - sigmu). --> sigmu(out[i]) * (1 - sigmu(out[i]));
// ...
// rember to convert x. or out[i-1] --> [x]. [out[i-1]] // math.multiply() need this other wies 
// we will get vector not array and we need array for it.
let backpropagation = (x, lr) => {
    for (let i = 0; i < ANN.get_lay()['lay_num']; i++) {
        let ers = [...ANN.get_error()[`error_${i}`]],
            outs = [...ANN.get_output()[`out_${i}`]],
            de = [], adj = [];

        // OPTIMIZE ...
        // ANN.isBias() && (i != ANN.get_lay()['lay_num'] - 1)
        if (ANN.isBias()) {
            x.unshift(1);
            if ((i != ANN.get_lay()['lay_num'] - 1)) {
                outs.shift();
            }
        }

        de = outs.map((out, idx) => lr * ers[idx] * ANN.sigmu_derivative(out));
        if (i != 0) {
            adj = math.transpose(math.multiply(de.map(x_ => [x_]), [ANN.get_output()[`out_${i - 1}`]]));
        } else {
            adj = math.transpose(math.multiply(de.map(x_ => [x_]), [x]));
        }
        ANN.set_weight_for(i, math.add(ANN.get_weight()[`w_${i}`], adj));
    }
};

let learn_ann = (data = [], y = [], leran_algo = 'S_SUCH_GRAD', lr = LEARNIG_RATE) => {
    LEARNIG_RATE = lr;
    ERROR_PER_EPO.leran_algo = leran_algo;

    switch (leran_algo) {
        case 'S_SUCH_GRAD':
            learn_scht_grad(data, y);
            break;
        default:
            break;
    }

};

// ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... //
//  Simpl suchastic gradint 
// param :
// 1. data:[], data for learn/test/validation;
// 2. y:[], desierd output;
// 3. EPO_NUM:number; number of EPOC.
// case : learing 
// 1. pass data/y to learn in array;
// 2. loop through the data array
// 3. each iteration DO -->
//    3.1. forword;
//    3.2. error signal;
//    3.3. backpropagation;
//    3.4. last error signal add to >> errors --> ERR_EPO:['EPO_NUM':{errors, SSE}];
// 4. at the end of iteration DO -->
//    4.1. count SSE and add to >> SSE --> ERR_EPO:['EPO_NUM':{errors, SSE}];
//    4.2. SE = 0.5 * SUM (target[i] - net_out[i] ); i -->  every iteration;
let learn_scht_grad = (data, y) => {
    for (let i = 0; i < data.length; i++) {
        forward(Object.values(data[i]));
        error_signal(Object.values(y[i]));
        backpropagation(Object.values(data[i]), LEARNIG_RATE);
        manage_error(
            ANN.get_error()[`error_${ANN.get_error()['error_num'] - 1}`],
            ANN.get_output()[`out_${ANN.get_output()['out_num'] - 1}`]
        );
    }
    squared_error();
    mean_squared_error();
    ERROR_PER_EPO.EPO_NUM += 1;
};

// .... FOR ONE OUTPUT NEU. UPDATE !!!  ... ///
let manage_error = (error, output) => {
    if (ERROR_PER_EPO[`epo_error_${ERROR_PER_EPO.EPO_NUM}`] === undefined) {
        ERROR_PER_EPO[`epo_error_${ERROR_PER_EPO.EPO_NUM}`] = [];
        ERROR_PER_EPO[`epo_out_${ERROR_PER_EPO.EPO_NUM}`] = [];
    }
    ERROR_PER_EPO[`epo_error_${ERROR_PER_EPO.EPO_NUM}`].push(error);
    ERROR_PER_EPO[`epo_out_${ERROR_PER_EPO.EPO_NUM}`].push(output);
};

// SE (Squared Error) 
// SE = 0.5 * SUM (target[i] - net_out[i] )^2;
let squared_error = () => ERROR_PER_EPO[`SE`].push(.5 * math.sum(math.subtract(get_out_and_error()[0], get_out_and_error()[1])[0].map(d => math.pow(d, 2))));

// MSE (Mean Squared Error)
let mean_squared_error = () => ERROR_PER_EPO[`MSE`].push(math.sum(math.subtract(get_out_and_error()[0], get_out_and_error()[1])[0].map(d => math.pow(d, 2))) / get_out_and_error()[0].length);

let get_out_and_error = () => {
    return [
        math.transpose(ERROR_PER_EPO[`epo_out_${ERROR_PER_EPO.EPO_NUM}`]),
        math.transpose(ERROR_PER_EPO[`epo_error_${ERROR_PER_EPO.EPO_NUM}`])
    ];
};

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //

export class ManageANN {
    // ...
    get_err_per_EPO = () => ERROR_PER_EPO;

    // ...
    build_ann = build_ann;

    learn_ann = learn_ann;
    // test_ann = test_ann;
    // validate_ann = validate_ann;
}
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //