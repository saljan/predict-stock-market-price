import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';
// import { FirebaseService } from "../providers/firebase.service";
// import { MetaTag } from "../models/meta-tag";


@Injectable()
export class SeoService {

  // ...
  REAL_TIME_DATA_BASE_REFERENCES: string = 'meta-tag';


  // ...
  // constructor(private FirebaseService: FirebaseService, private Meta: Meta) { }


  // ...
  setTagsFor(page_name) {
    switch (page_name) {
      case 'home':
        // this.FirebaseService.getDataByKey(this.REAL_TIME_DATA_BASE_REFERENCES, page_name).subscribe(d => this.generateTags(d[0]));
        break;
      default:
        break;
    }
  }


  // private ...
  private generateTags(config) {
    // default values
    config = {
      author: 'salem albarudy',
      basic_desc: 'salem albarudy',
      keywords: ['js', 'java', 'puthon'],

      title: 'Angular <3 Linkbots',
      description: 'My SEO friendly Angular Component',
      image: 'https://angularfirebase.com/images/logo.png',
      slug: '',

      ...config
    }

    // ...
    this.updateBasicTag(config);
    this.updateTagForTwitter(config);
    this.updateTagForFaceBook(config);

  }

  private updateBasicTag(config) {
    // this.Meta.updateTag({ name: 'author', content: config.author });
    // this.Meta.updateTag({ name: 'description', content: config.basic_desc });
    // this.Meta.updateTag({ name: 'keywords', content: config.keywords });
  }

  private updateTagForTwitter(config) {
    // this.Meta.updateTag({ name: 'twitter:card', content: 'summary' });
    // this.Meta.updateTag({ name: 'twitter:site', content: '@angularfirebase' });
    // this.Meta.updateTag({ name: 'twitter:title', content: config.title });
    // this.Meta.updateTag({ name: 'twitter:description', content: config.description });
    // this.Meta.updateTag({ name: 'twitter:image', content: config.image });
  }

  private updateTagForFaceBook(config) {
    // this.Meta.updateTag({ property: 'og:type', content: 'article' });
    // this.Meta.updateTag({ property: 'og:site_name', content: 'AngularFirebase' });
    // this.Meta.updateTag({ property: 'og:title', content: config.title });
    // this.Meta.updateTag({ property: 'og:description', content: config.description });
    // this.Meta.updateTag({ property: 'og:image', content: config.image });
    // this.Meta.updateTag({ property: 'og:url', content: `https://instafire-app.firebaseapp.com/${config.slug}` });
  }


}
