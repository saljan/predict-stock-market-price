import { Injectable } from '@angular/core';
import { AnnManagerService } from "../ann-manager/ann-manager.service";
import { DataManagerService } from "../data-manager.service";
import * as Plotly from 'plotly.js';

@Injectable()
export class ChartService {

  // ...
  current_elements: any = {};

  // ...
  constructor() { }

  // ...
  draw_output(key, data, ele) {
    // console.log('DRAW');
    // console.log(data);
    // console.log(data.desired);
    // console.log(data.out);
    // if (data.out.length === 0) return; // !!!
    this.current_elements[key] = ele.nativeElement;
    const DESIERD = {
      x: data.desired.map(d => d.date[0]),
      y: data.desired.map(d => d.price_norm[0]),
      name: 'desierd',
      line: {
        color: 'red'
      }
    };
    const ANN_OUT = {
      x: data.desired.map(d => d.date[0]),
      y: data.out[0],
      name: 'ann output',
      line: {
        color: 'blue'
      }
    };
    const formattedData = [DESIERD, ANN_OUT];

    const style = {
      margin: { t: 60 },
      title: 'ANN output',
      xaxis: { title: 'Date' },
      yaxis: { title: 'Price ($)' }
    }

    Plotly.plot(this.current_elements[key], formattedData, style);
    // Plotly.newPlot(this.current_elements[key], formattedData, style)
  }

  draw_new_output(data, ele) {
    // console.log('draw_new_output');
    // console.log(data);
    // console.log(data.desired);
    // console.log(data.out);
    // console.log(ele);
    if (data.out.length === 0) return; // !!!    
    const DESIERD = {
      x: data.desired.map(d => d.date[0]),
      y: data.desired.map(d => d.price_norm[0]),
      name: 'Desierd Output',
      line: {
        color: 'red'
      }
    };
    const ANN_OUT = {
      x: data.desired.map(d => d.date[0]),
      y: data.out[0],
      name: 'ANN Output',
      line: {
        color: 'blue'
      }
    };
    const formattedData = [DESIERD, ANN_OUT];

    const style = {
      margin: { t: 60 },
      title: 'ANN output',
      xaxis: { title: 'Date' },
      yaxis: { title: 'Price ($)' }
    }

    Plotly.newPlot(ele.nativeElement, formattedData, style)
  }


  draw_errors(key, data, ele, title = 'Errors after learn') {
    // console.log(data);
    // console.log(ele);
    // if (data.MSE.length === 0) return;
    this.current_elements[key] = ele.nativeElement;
    const MSE = {
      y: typeof data.MSE === 'object' ? data.MSE : [1, data.MSE],
      name: 'MSE',
      line: {
        color: 'red'
      }
    };
    const RMSE = {
      y: typeof data.RMSE === 'object' ? data.RMSE : [1, data.RMSE],
      name: 'RMSE',
      line: {
        color: 'blue'
      }
    };
    const SSE = {
      y: data.SSE,
      name: 'SSE',
      line: {
        color: 'orange'
      }
    };
    const formattedData = [MSE, RMSE, SSE];
    const style = {
      margin: { t: 60 },
      width: 500,
      height: 500,
      title,
      xaxis: { title: 'EPOCS' }
      // yaxis: { title: 'Price ($)' }
    }

    Plotly.newPlot(this.current_elements[key], formattedData, style)
  }

  update(key, data = []) {
    // console.log(this.current_elements);
    // console.log(this.current_elements[key]);
    console.log('UPDATE');
    Plotly.extendTraces(this.current_elements[key], { x: data }, [0]);
  }

  update_output(key, data) {
    console.log('update_output');
    console.log(data);
    console.log(data.out[0]);
    console.log(key);
    Plotly.extendTraces(this.current_elements[key], { x: data.out[0] }, [0]);
  }

  update_errors(key, data) {
    // console.log('update_output');
    // console.log(data);
    // console.log(data.MSE);
    // console.log(this.current_elements[key]);
    Plotly.extendTraces(
      this.current_elements[key], { y: [[data.MSE], [data.RMSE], [data.SSE]] }, [0, 1, 2]);
  }

}
