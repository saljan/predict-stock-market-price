import { TestBed, inject } from '@angular/core/testing';

import { AnnDrawerService } from './ann-drawer.service';

describe('AnnDrawerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnnDrawerService]
    });
  });

  it('should be created', inject([AnnDrawerService], (service: AnnDrawerService) => {
    expect(service).toBeTruthy();
  }));
});
