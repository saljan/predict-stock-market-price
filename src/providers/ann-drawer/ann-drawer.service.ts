import { Injectable } from '@angular/core';
import * as d3 from 'd3';

@Injectable()
export class AnnDrawerService {

  constructor() { }


  // ...
  draw_neurals = (options, inputs, layers, outputs, value = []) => {
    // ...
    let color = d3.scaleOrdinal(d3.schemeCategory10),
      svg = options.svg,
      data = this.get_data(inputs, layers, outputs);

    // ...
    let nodes = data.nodes;
    let netsize = this.get_network_size(nodes);
    let largestLayerSize = this.largest_layer_size(netsize);
    let xdist = options.width / Object.keys(netsize).length,
      ydist = options.height / largestLayerSize,
      node_max_size = 30;


    this.nodes_locations(nodes, xdist, ydist);
    let links_data = this.generate_links(nodes, value);

    let print_info_on = (d) => {
      svg.append('g')
        .attr('transform', 'translate(' + 5 + ',' + 5 + ')')
        .append("text")
        .attr('class', 'info_box')
        .attr("dx", "-.35em")
        .attr("dy", ".35em")
        .text(`w : ${d.value} `);
    };

    let print_info_out = (d) => {
      d3.select('.info_box').remove();
    };

    // let box

    // draw links
    let link = svg.selectAll(".link")
      .data(links_data)
      .enter()
      .append("line")
      .attr("class", "link")
      .attr("x1", (d) => nodes[d.source].x)
      .attr("y1", (d) => nodes[d.source].y)
      .attr("x2", (d) => nodes[d.target].x)
      .attr("y2", (d) => nodes[d.target].y)
      .style("stroke-width", (d) => d.value * options.stroke)
      .on("mouseover", print_info_on)
      .on("mouseout", print_info_out);



    // draw nodes
    let node = svg.selectAll(".node")
      .data(nodes)
      .enter().append("g")
      .attr("transform", (d) => "translate(" + d.x + "," + d.y + ")");

    let circle = node.append("circle")
      .attr("class", "node")
      .attr("r", node_max_size)
      .style("fill", (d) => color(d.layer));


    node.append("text")
      .attr("dx", "-.35em")
      .attr("dy", ".35em")
      .text((d) => d.label);

  };

  // private ...

  private get_data(inputs, layers, outputs) {
    let neural_data = { nodes: [] };
    for (let i = 0; i < inputs; i++)
      neural_data.nodes.push({ "label": `i${i}`, "layer": 1, value: 0 });
    for (let i in layers)
      for (let j = 0; j < layers[i]; j++)
        neural_data.nodes.push({ "label": `i${j}`, "layer": +i + 2, value: 0 });
    for (let i = 0; i < outputs; i++)
      neural_data.nodes.push({ "label": `i${i}`, "layer": layers.length + 2, value: 0 });
    // console.log(neural_data)
    return neural_data;
  };

  private get_network_size(nodes) {
    let netsize = {};
    nodes.forEach(function (d) {
      if (d.layer in netsize) {
        netsize[d.layer] += 1;
      } else {
        netsize[d.layer] = 1;
      }
      d["lidx"] = netsize[d.layer];
    });

    return netsize;
  };

  // calc distances between nodes
  private largest_layer_size(netsize) {
    return Math.max.apply(null, Object.keys(netsize).map((i) => netsize[i]));
  }

  private nodes_locations(nodes, xdist, ydist) {
    return nodes.map((d) => {
      d["x"] = (d.layer - 0.5) * xdist;
      d["y"] = (d.lidx - 0.5) * ydist;
    });
  }

  private generate_links(nodes, value = []) {
    let links_data = [], x = [];
    nodes.map((d, i) => {
      for (let n in nodes) {
        if (d.layer + 1 == nodes[n].layer) {
          links_data.push({ "source": parseInt(i), "target": parseInt(n), "value": .5 })
          // links_data.push({ "source": parseInt(i), "target": parseInt(n), "value": Math.random() * 10 })
        }
      }
    }).filter((d) => typeof d !== "undefined");

    links_data.map((d, i) => d.value = value[i]);

    return links_data;
  };
}
