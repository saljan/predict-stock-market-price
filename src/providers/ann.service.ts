import { Injectable } from '@angular/core';
import { DataSet } from "../model/data-set";
import { ANN } from "../model/ann";
import { DataManagerService } from "./data-manager.service";
import { API } from "../core/API";
import * as d3 from "d3";
import * as math from "mathjs";



@Injectable()
export class AnnService {

  // ...
  private selectedDataSet: DataSet = new DataSet();
  private selectedANN: ANN = new ANN();
  private selectedLearnigAlgo = {
    learning_algo: 'S_SUCH_GRAD',
    learning_rate: .4,
    epoc: 10
  };
  API: API = new API();

  private learning_data = [];
  private testing_data = [];
  private valiation_data = [];
  private desired_output = [];

  private MSE_LR = [];
  private SE_LR = [];

  private MSE_TS = [];
  private SE_TS = [];

  private MSE_VL = [];
  private SE_VL = [];

  private output_after_learn = [];
  private output_after_test = [];
  private output_after_validation = [];
  data; // back to private

  // ...
  constructor(private DataManagerService: DataManagerService) {}

  // 1. TODO 
  buildANN() {
    this.data = this.DataManagerService.getData({
      stock: this.selectedDataSet.stock,
      start: this.selectedDataSet.start,
      end: this.selectedDataSet.end
    }, {
        input_num: this.selectedANN.input_num,
        output_num: this.selectedANN.output_num
      });

    let neu_num = [+this.selectedANN.input_num];//console.log(this.selectedANN)
    this.selectedANN['layers_num'].split(';').map(d => neu_num.push(+d)); // split error because it number no string make dynamic form 
    neu_num.push(+this.selectedANN.output_num);
    this.API.build_ann(neu_num, this.selectedANN.bias, this.selectedANN.actv_func);

    console.log(this.data); // ****
  }

  // 2.
  learnANN() {
    let keys = 'percentage_of_change';
    if (this.selectedDataSet.price) keys = 'price_norm';

    this.learning_data = this.data.learning.map(d => d[keys]);
    this.desired_output = this.data.desierd_output_learning.map(d => d[keys])

    for (let i = 0; i < this.selectedLearnigAlgo.epoc; i++) {
      this.API.learn_ann(this.learning_data, this.desired_output, this.selectedLearnigAlgo.learning_algo, this.selectedLearnigAlgo.learning_rate);
    }

    this.output_after_learn = math.transpose(this.API.get_err_per_EPO()[`epo_out_${this.API.get_err_per_EPO().EPO_NUM - 1}`]);
  }

  // 3.
  testANN(arg) {
    let keys = 'percentage_of_change';
    if (this.selectedDataSet.price) keys = 'price';

    this.testing_data = this.data.testing.map(d => d[keys]);
    this.desired_output = this.data.desierd_output_testing.map(d => d[keys])

    for (let i = 0; i < this.selectedLearnigAlgo.epoc; i++) {
      // WE FIX IT, TEST func JUST TEST NO MORE LERAN
      
      // this.API.test_ann(
      //   this.testing_data,
      //   this.desired_output,
      //   this.selectedLearnigAlgo.learning_algo,
      //   this.selectedLearnigAlgo.learning_rate);
    }

    this.output_after_test = math.transpose(this.API.get_test_err_per_EPO()[`epo_out_${this.API.get_test_err_per_EPO().EPO_NUM - 1}`]);
  }

  // 4.
  validateANN(arg) {
    let keys = 'percentage_of_change';
    if (this.selectedDataSet.price) keys = 'price';

    this.valiation_data = this.data.validation.map(d => d[keys]);
    this.desired_output = this.data.desierd_output_validation.map(d => d[keys])

    for (let i = 0; i < this.selectedLearnigAlgo.epoc; i++) {
      this.API.validate_ann(arg,
        this.valiation_data,
        this.desired_output,
        this.selectedLearnigAlgo.learning_algo,
        this.selectedLearnigAlgo.learning_rate);
    }

    this.MSE_VL = this.API.get_validate_err_per_EPO().MSE;
    this.SE_VL = this.API.get_validate_err_per_EPO().SE;
    this.output_after_validation = math.transpose(this.API.get_validate_err_per_EPO()[`epo_out_${this.API.get_validate_err_per_EPO().EPO_NUM - 1}`]);

    console.log(this.API.get_err_per_EPO())
    console.log(this.API.get_test_err_per_EPO())
    console.log(this.API.get_validate_err_per_EPO())
  }

  // 5.
  feedANN() { }

  // GET/SET ...

  getMSE() {
    return this.API.get_MSE();
  }

  getRMSE() {
    return this.API.get_RMSE();
  }

  getSSE() {
    return this.API.get_SSE();
  }

  setSelectedData(arg: DataSet) {
    arg.start = typeof arg.start === 'object' ? this.dateFormat(arg.start) : arg.start;
    arg.end = typeof arg.end === 'object' ? this.dateFormat(arg.end) : arg.end;
    this.selectedDataSet = arg;
  }

  getSelectedData() {
    return this.selectedDataSet;
  }

  setSelectedANN(arg: ANN) {
    this.fetchdata(arg); // STILL NOT WORKING 
  }

  getSelectedANN(): ANN {
    return this.selectedANN;
  }

  setSelectedLearningAlgo(arg) {
    this.selectedLearnigAlgo = arg;
  }

  getSelectedLearningAlgo() {
    return this.selectedLearnigAlgo;
  }

  getMSE_LR() {
    return this.MSE_LR;
  }

  getSE_LR() {
    return this.SE_LR;
  }

  getMSE_TS() {
    return this.MSE_TS;
  }

  getSE_TS() {
    return this.SE_TS;
  }

  getMSE_VL() {
    return this.MSE_VL;
  }

  getSE_VL() {
    return this.SE_VL;
  }

  getOutputAfterLearn() {
    return this.output_after_learn;
  }

  getOutputAfterTesting() {
    return this.output_after_test;
  }

  getOutputAfterValidation() {
    return this.output_after_validation;
  }

  // PRIVATE ...
  private fetchdata(arg) {
    arg.layers_num = arg.layers_num.split(';');
    this.selectedANN = arg;
    console.log(this.selectedANN)
  }

  private dateFormat(date) {
    let mm = date.getMonth() + 1; // getMonth() is zero-based
    let dd = date.getDate();

    return [
      date.getFullYear(),
      '-' + (mm > 9 ? '' : '0') + mm,
      '-' + (dd > 9 ? '' : '0') + dd
    ].join('');
  }
}
