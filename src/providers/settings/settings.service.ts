import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class SettingsService {

  // ...
  data_set_tab = new BehaviorSubject(false);
  neural_tab = new BehaviorSubject(false);
  // simulation_tab = new BehaviorSubject(false);
  simulation_tab = new BehaviorSubject(true);

  // ...
  constructor() { }

  // ...
  change_dataset_tab() {
    this.data_set_tab.next(!this.data_set_tab.value);
  }

  change_neural_tab() {
    this.neural_tab.next(!this.neural_tab.value);
  }

  change_simulation_tab(val) {
    this.simulation_tab.next(val);
  }

}
