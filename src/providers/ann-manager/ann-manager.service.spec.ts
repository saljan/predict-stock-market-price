import { TestBed, inject } from '@angular/core/testing';

import { AnnManagerService } from './ann-manager.service';

describe('AnnManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnnManagerService]
    });
  });

  it('should be created', inject([AnnManagerService], (service: AnnManagerService) => {
    expect(service).toBeTruthy();
  }));
});
