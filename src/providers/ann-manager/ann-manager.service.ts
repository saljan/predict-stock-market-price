import { Injectable } from '@angular/core';
import { DataManagerService } from "../data-manager.service";
import { ChartService } from "../chart/chart.service";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { timer } from 'rxjs/observable/timer';
import { take, map } from "rxjs/operators";
import { DataSet } from "../../model/data-set";
import { ANN } from "../../model/ann";
import { API } from "../../core/API";
import * as math from "mathjs";
import * as Plotly from 'plotly.js';


@Injectable()
export class AnnManagerService {

  // ...
  weights_for_simulation = [];
  private selectedDataSet = new BehaviorSubject<DataSet>(new DataSet());
  curentDataSet = this.selectedDataSet.asObservable();
  private DATA;
  private selectedANN: ANN = new ANN();
  private selectedLearnigAlgo = {
    learning_algo: 'Back_Propagation', // change name to back-probagation
    learning_rate: .4,
    epoc: 1000,
    error_tolerance: 0.01,
    iteration_interval: 0
  };
  private API: API = new API();
  private output_after_learn;
  private stop;
  private EPOC_NUM = new BehaviorSubject<number>(0);
  current_EPOC_NUM = this.EPOC_NUM.asObservable();
  // ...
  private ERR_LR = new BehaviorSubject<{ MSE: any[], RMSE: any[], SSE: any[] }>({ MSE: [], RMSE: [], SSE: [] });
  curent_ERR_LR = this.ERR_LR.asObservable();
  private ERR_TS = new BehaviorSubject<{ MSE: any[], RMSE: any[], SSE: any[] }>({ MSE: [], RMSE: [], SSE: [] });
  curent_ERR_TS = this.ERR_TS.asObservable();
  private ANN_OUT_LR = new BehaviorSubject<{ desired: any[], out: any[] }>({ desired: [], out: [] });
  curent_ANN_OUT_LR = this.ANN_OUT_LR.asObservable();
  private ANN_OUT_TS = new BehaviorSubject<{ desired: any[], out: any[] }>({ desired: [], out: [] });
  curent_ANN_OUT_TS = this.ANN_OUT_TS.asObservable();


  // ...
  constructor(private chartService: ChartService, private DataManagerService: DataManagerService) { }

  // Work on ANN ...
  // 0. 
  buildANN() {
    this.DATA = this.initDATA();

    let neu_num = [+this.selectedANN.input_num];//console.log(this.selectedANN)
    this.selectedANN['layers_num'].split(';').map(d => neu_num.push(+d)); // split error because it number no string make dynamic form 
    neu_num.push(+this.selectedANN.output_num);
    this.API.build_ann(neu_num, this.selectedANN.bias, this.selectedANN.actv_func);
    //    
  }

  reset() {
    this.selectedDataSet.next(new DataSet());
    this.selectedANN = new ANN();
    this.selectedLearnigAlgo = {
      learning_algo: 'Back_Propagation', // change name to back-probagation
      learning_rate: .4,
      epoc: 1000,
      error_tolerance: 0.01,
      iteration_interval: 0
    };
  }

  // 1.
  learnANN(key = { out: {}, err: '' }) {
    let keys = 'price_norm';
    let input = this.DATA.learning.map(d => d[keys]),
      output = this.DATA.desierd_output_learning.map(d => d[keys]),
      epoc = this.selectedLearnigAlgo.epoc;

    // iteration_interval is secounds chang to milisec. --> 1 sec. = 1000 milisec.
    this.stop = timer(0, this.selectedLearnigAlgo.iteration_interval * 1000)
      .pipe(take(epoc), map(() => --epoc))
      .subscribe(d => {
        this.API.learn_ann(input, output, this.selectedLearnigAlgo.learning_algo, this.selectedLearnigAlgo.learning_rate);
        let ann_out = math.transpose(this.API.get_err_per_EPO()[`epo_out_${this.API.get_err_per_EPO().EPO_NUM - 1}`]);
        this.increaseEPOC();
        this.set_w_for_simulation();
        this.chartService.draw_new_output({ desired: this.DATA.desierd_output_learning, out: ann_out }, key.out);
        this.chartService
          .update_errors(key.err, {
            MSE: this.API.get_MSE_FOR('LEARNING'),
            RMSE: this.API.get_RMSE_FOR('LEARNING'),
            SSE: this.API.get_SSE_FOR('LEARNING')
          });        
        if (this.API.get_RMSE_FOR('LEARNING') < this.selectedLearnigAlgo.error_tolerance) this.stop_proccess();
      });
  }

  // 2.
  testANN(key = { out: {}, err: {} }) {
    let keys = 'price_norm';

    let input = this.DATA.testing.map(d => d[keys]);
    let output = this.DATA.desierd_output_testing.map(d => d[keys]);
    this.API.test_ann(input, output, this.selectedLearnigAlgo.learning_algo);


    let ann_out = math.transpose(this.API.get_test_err_per_EPO()[`epo_out_${this.API.get_test_err_per_EPO().EPO_NUM - 1}`]);
    this.chartService.draw_new_output({ desired: this.DATA.desierd_output_testing, out: ann_out }, key.out);
    this.chartService.draw_errors('el_TS_ER', {
      MSE: this.API.get_MSE_FOR('TESTING'),
      RMSE: this.API.get_RMSE_FOR('TESTING'),
      SSE: this.API.get_SSE_FOR('TESTING')
    }, key.err, 'Errors after Testing');


    // this.chartService
    //   .update_errors(key.err, {
    //     MSE: this.API.get_MSE_FOR('TESTING'),
    //     RMSE: this.API.get_RMSE_FOR('TESTING'),
    //     SSE: this.API.get_SSE_FOR('TESTING')
    //   });

    // for (let epoc = 0; epoc < this.selectedLearnigAlgo.epoc; epoc++) {
    //   this.manageTestErrors();
    //   this.mange_ANN_OUT_TS();
    // }
  }

  // 3.  NOW NOT USING VALD / JUST LEARN AND TEST
  validateANN() {
    let keys = 'price_norm';

    let input = this.DATA.validation.map(d => d[keys]);
    let output = this.DATA.desierd_output_validation.map(d => d[keys]);

    for (let epoc = 0; epoc < this.selectedLearnigAlgo.epoc; epoc++) {
      // this.API.test_ann(input, output, this.selectedLearnigAlgo.learning_algo);
      // this.manageTestErrors();
      // this.mange_ANN_OUT_TS();
    }
  }

  // Get/Set ...
  setDataSet(dataset) {
    this.selectedDataSet.next(dataset);
  }

  setSelectedANN(arg: ANN) {
    this.fetchdata(arg); // FIX THIS in  future for many layers dynamiclly
  }

  getSelectedANN(): ANN {
    return this.selectedANN;
  }

  setSelectedLearningAlgo(arg) {
    this.selectedLearnigAlgo = arg;
  }

  getSelectedLearningAlgo() {
    return this.selectedLearnigAlgo;
  }

  initDATA() {
    return this.DataManagerService.getData({
      stock: this.selectedDataSet.value.stock,
      start: this.selectedDataSet.value.start,
      end: this.selectedDataSet.value.end
    }, {
        input_num: this.selectedANN.input_num,
        output_num: this.selectedANN.output_num
      });
  }

  getDATA() {
    return this.DATA;
  }

  get_weight_info() {
    return this.API.get_ann().get_weight();
  }

  set_w_for_simulation() {
    let tmp = [], value = this.API.get_ann().get_weight();
    for (let i = 0; i < value['w_num']; i++)
      tmp = tmp.concat(...value[`w_${i}`]);
    this.weights_for_simulation.push(tmp);
  }

  // ...
  increaseEPOC() {
    this.EPOC_NUM.next(this.EPOC_NUM.value + 1);
  }

  stop_proccess() {
    this.stop.unsubscribe();
    console.log('MSE : ', this.API.get_MSE_FOR('LEARNING'));
    console.log('RMSE : ', this.API.get_RMSE_FOR('LEARNING'));
    console.log('SSE : ', this.API.get_SSE_FOR('LEARNING'));
    console.log('STOP.')
  }

  manageErrors() {
    let mse = this.API.get_MSE_FOR('LEARNING'),
      rmse = this.API.get_RMSE_FOR('LEARNING'),
      sse = this.API.get_SSE_FOR('LEARNING');
    // let mse = this.API.get_MSE(),
    //   rmse = this.API.get_RMSE(),
    //   sse = this.API.get_SSE();

    // this.API.setMSE(mse);
    // this.API.setRMSE(rmse);
    // this.API.setSSE(sse);

    this.ERR_LR.value.MSE.push(mse);
    this.ERR_LR.value.RMSE.push(rmse);
    this.ERR_LR.value.SSE.push(sse);
    this.ERR_LR.next(this.ERR_LR.value);
  }

  manageTestErrors() {
    let mse = this.API.get_MSE_FOR('TESTING'),
      rmse = this.API.get_RMSE_FOR('TESTING'),
      sse = this.API.get_SSE_FOR('TESTING');

    // this.API.setMSE(mse);
    // this.API.setRMSE(rmse);
    // this.API.setSSE(sse);

    this.ERR_TS.value.MSE.push(mse);
    this.ERR_TS.value.RMSE.push(rmse);
    this.ERR_TS.value.SSE.push(sse);
    this.ERR_TS.next(this.ERR_TS.value);
  }

  mange_ANN_OUT() {
    this.ANN_OUT_LR.value.desired = this.DATA.desierd_output_learning;
    this.ANN_OUT_LR.value.out = math.transpose(this.API.get_err_per_EPO()[`epo_out_${this.API.get_err_per_EPO().EPO_NUM - 1}`]);
    this.ANN_OUT_LR.next(this.ANN_OUT_LR.value);
  }

  mange_ANN_OUT_TS() {

    this.ANN_OUT_TS.value.desired = this.DATA.desierd_output_testing;
    this.ANN_OUT_TS.value.out = math.transpose(this.API.get_test_err_per_EPO()[`epo_out_${this.API.get_test_err_per_EPO().EPO_NUM - 1}`]);
    this.ANN_OUT_TS.next(this.ANN_OUT_TS.value);
  }

  // PRIVATE ...
  private fetchdata(arg) {
    arg.layers_num = arg.layers_num.split(';');
    this.selectedANN = arg;
    // console.log(this.selectedANN)
  }

}
