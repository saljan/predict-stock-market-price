import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// ...
import { SeoService } from "./seo.service";
import { DataManagerService } from "./data-manager.service";
import { SettingsService } from "./settings/settings.service";
import { AnnService } from "./ann.service";
import { AnnDrawerService } from "./ann-drawer/ann-drawer.service";

import { AnnManagerService } from "./ann-manager/ann-manager.service";
import { ChartService } from "./chart/chart.service";


@NgModule({
  imports: [
    HttpClientModule,
  ],
  declarations: [],
  providers: [
    SettingsService, 
    SeoService, 
    ChartService,
    DataManagerService,
    AnnService, // we dont need this one 
    AnnManagerService,
    AnnDrawerService
  ]
})
export class ProvidersModule { }
