import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as d3 from "d3";
import * as math from "mathjs";


const DATA_STRUCURE = { price: [], price_norm: [], date: [], percentage_of_change: [], percentage_of_change_norm: [] };
const tmp_stock_logo = [
  { index: 'BTC', name: 'BitCoin', url: 'https://bit.ly/2EFxzQr' },
  { index: 'ETH', name: 'Ethereum', url: 'https://bit.ly/2IQWcfj' },
  { index: 'XRP', name: 'Ripple', url: 'https://bit.ly/2HiOeOV' }
];


@Injectable()
export class DataManagerService {

  // ...
  // This simp. -->( :: )<-- mean the same type and structure like right side.
  // dataset :: [ { price:number, date:yyy-mm-dd, percentage_of_change:number } ]
  // learning :: [{ price_in:[num_of_in], pers_in:[num_of_in], data:[num_of_in] } ]
  // learning :: testing :: validation
  // desierd_output_learning :: [ {price_out:[num_of_out], pers_out:[num_of_out], data:[num_of_out}]
  private data = {
    dataset: JSON.parse(JSON.stringify(DATA_STRUCURE)),
    learning: JSON.parse(JSON.stringify(DATA_STRUCURE)),
    testing: JSON.parse(JSON.stringify(DATA_STRUCURE)),
    validation: JSON.parse(JSON.stringify(DATA_STRUCURE)),
    desierd_output_learning: JSON.parse(JSON.stringify(DATA_STRUCURE)),
    desierd_output_testing: JSON.parse(JSON.stringify(DATA_STRUCURE)),
    desierd_output_validation: JSON.parse(JSON.stringify(DATA_STRUCURE)),
    options: {}
  };



  // ...
  constructor(private _http: HttpClient) { }

  getData(data_info = { stock: 'BTC', start: '2017-01-01', end: '2017-12-31' }, options = {}) {
    let tmp;
    this.data.options = {
      input_num: 2,
      output_num: 1,
      divid_percentage: .6,
      normalized: true,
      shuffled: false,
      ...options
    };

    switch (data_info.stock) {
      case 'BTC':
        tmp = this.get_btc_data(data_info.start, data_info.end);
        break;
      case 'XRP':
        break;
    }

    this.initData(tmp);

    return this.data;
  }

  get_btc_data(start = '2017-01-01', end = '2017-12-31') {
    return this._http.get(this.get_url_btc(start, end)).map(result => result);
  }


  getStocks() {
    return tmp_stock_logo;
  }

  // ...
  // 
  initData(data) {
    // console.log(this.data.dataset)

    data.subscribe(res => {
      let price = Object.values(res['bpi']),
        date = Object.keys(res['bpi']);

      this.data.dataset = { price, date, price_norm: [], percentage_of_change: [] };

      this.data.dataset.price.forEach((p, i) => {
        this.data.dataset.price_norm[i] = this.normalize(price, p);
        this.data.dataset.percentage_of_change[i] = i != 0 ? (p - price[i - 1]) / price[i - 1] : 0;
      });

      this.divided_data_set(this.data.dataset);
      this.make_inputs_array(this.data.options['input_num'], 'learning');
      this.make_inputs_array(this.data.options['input_num'], 'testing');
      this.make_inputs_array(this.data.options['input_num'], 'validation');
    });
  }

  //
  divided_data_set(data, percentage = this.data.options['divid_percentage']) {
    let len = data.price_norm.length,
      ler_data = len * percentage,
      other = (len - ler_data) / 2;

    data.price_norm.map((p, i) => {
      if (i >= 0 && i <= ler_data) {
        this.data.learning.price_norm.push(p);
        this.data.learning.price.push(data.price[i]);
        this.data.learning.date.push(data.date[i]);
        this.data.learning.percentage_of_change.push(data.percentage_of_change[i]);
      }
      if (i > ler_data && i <= (ler_data + other)) {
        this.data.testing.price_norm.push(p);
        this.data.testing.price.push(data.price[i]);
        this.data.testing.date.push(data.date[i]);
        this.data.testing.percentage_of_change.push(data.percentage_of_change[i]);
      }
      if (i > (ler_data + other) && i <= (ler_data + other + other)) {
        this.data.validation.price_norm.push(p);
        this.data.validation.price.push(data.price[i]);
        this.data.validation.date.push(data.date[i]);
        this.data.validation.percentage_of_change.push(data.percentage_of_change[i]);
      }
    });
  }

  make_inputs_array = (input_num, key) => {
    let tmp = JSON.parse(JSON.stringify(this.data[key]));
    this.data[key] = [];
    this.data[`desierd_output_${key}`] = [];

    for (let i = 0; i < tmp.price.length - this.data.options['input_num']; i++) {
      let xPrice = tmp.price.slice(i, i + input_num),
        xPrice_norm = tmp.price_norm.slice(i, i + input_num),
        xPercent = tmp.percentage_of_change.slice(i, i + input_num),
        xDate = tmp.date.slice(i, i + input_num),
        yPrice = this.make_output_array(tmp.price, i + input_num),
        yPrice_norm = this.make_output_array(tmp.price_norm, i + input_num),
        yPercent = this.make_output_array(tmp.percentage_of_change, i + input_num),
        yDate = this.make_output_array(tmp.date, i + input_num);

      if (xPrice.length < input_num) break;
      if (yPrice.length < this.data.options['output_num']) break;
      this.data[key].push({ price: xPrice, price_norm: xPrice_norm, date: xDate, percentage_of_change: xPercent });
      this.data[`desierd_output_${key}`].push({ price: yPrice, price_norm: yPrice_norm, date: yDate, percentage_of_change: yPercent });
    }
  }

  make_output_array = (data, i) => {
    return data.slice(i, i + this.data.options['output_num']);
  }

  //
  shuffle = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  };


  //  x` = x - min / max - min
  normalize(data, x) {
    return (x - (+d3.min(data))) / (+d3.max(data) - +d3.min(data));
  }

  // x * ( max - min) + min
  denormalize(x, key = 'percentage_of_change') {
    return x * (+d3.max(this.data.dataset.price) - +d3.min(this.data.dataset.price)) + d3.min(this.data.dataset.price);
  }

  // ...
  // ex. https://api.coindesk.com/v1/bpi/historical/close.json?start=2017-01-01&end=2017-01-10
  private get_url_btc(start = '2017-01-01', end = '2017-12-31'): string {
    return `https://api.coindesk.com/v1/bpi/historical/close.json?start=${start}&end=${end}`;
  }

}
