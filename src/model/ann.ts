export class ANN {
    // (neu_arr = [1, 1], bias = true, actv = 'sigmu') 

    // ...
    input_num: number;
    output_num: number;
    // layers_num: number;
    layers_num: string;
    bias: boolean;
    actv_func: string;

    // ...
    constructor(
        input_num: number = 8,
        output_num: number = 1,
        // layers_num: number = 0,
        layers_num= '4;4',
        bias: boolean = true,
        actv_func: string = 'sigmu'
    ) {
        this.input_num = input_num;
        this.output_num = output_num;
        this.layers_num = layers_num;
        this.bias = bias;
        this.actv_func = actv_func;
    }
}