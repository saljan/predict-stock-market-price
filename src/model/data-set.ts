export class DataSet {
    // ...
    stock?: string;
    start: string;
    end: string;
    price?: boolean;
    percenteg?: boolean;
    micro_date?: boolean;

    // ...
    constructor(stock = 'BTC', start = '2017-01-01', end = '2017-12-31') {
        this.stock = stock;
        this.start = start;
        this.end = end;
        this.percenteg = false;
        this.micro_date = false;
        this.price = true;
    }
}